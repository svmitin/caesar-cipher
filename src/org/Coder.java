package org;

public class Coder {
    /** Кодер-декодер шифров Цезаря */

    public String encodeString(String string, int delta) {
        /* Зашифровать строку используя в качестве ключа дельту */
        return decodeString(string, delta * -1);
    }

    public String encodeString(String string, int[] wordDeltas) {
        /* Зашифровать строку используя в качестве ключа массив дельт */
        int[] reverseWordDeltas = new int[wordDeltas.length];

        for (int i = 0; i < wordDeltas.length; i++) {
            reverseWordDeltas[i] = wordDeltas[i] * -1;
        }

        return decodeString(string, reverseWordDeltas);
    }

    public String decodeString(String string, int delta) {
        /* Расшифровать строку используя в качестве ключа дельту */
        StringBuilder decoded = new StringBuilder();

        Alphabet alphabet = new Alphabet();
        char[] array = string.toCharArray();


        for (char symbol : array) {
            decoded.append(alphabet.changeSymbol(symbol, delta));
        }

        return decoded.toString();
    }

    public String decodeString(String string, int[] wordDeltas) {
        /* Расшифровать строку используя в качестве ключа массив дельт */
        StringBuilder decoded = new StringBuilder();
        int wordDeltasIndex = 0;

        Alphabet alphabet = new Alphabet();
        String[] words = string.split(" ");

        char[] array = string.toCharArray();
        for (char symbol : array) {
            if (symbol == ' ') {
                wordDeltasIndex++;
            }
            decoded.append(alphabet.changeSymbol(symbol, wordDeltas[wordDeltasIndex]));
        }

        return decoded.toString();
    }
}