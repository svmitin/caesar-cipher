package org;


public class Main {
    public static void main(String[] args) {

        Coder coder = new Coder();

        System.out.println(coder.decodeString("ЗТЕФТИ ЧЦФТ!", -4));
        System.out.println(coder.decodeString("ЙЛЗ Б ЗЦИЭ Й ГЖКДЭКГШЕБ Ъ НЖДЖЬБДФЁБГЭ.", 8));
        System.out.println(coder.decodeString("БЪВАШЬЪ УГЦ ЯЦ ЦЙН УЦИЦВАЮ БВЪХЕД ФАГДЪ.", 15));
        System.out.println(coder.decodeString("ЗЛКСВПЗЁ Н ЁЫНЫДТЫ А ЩЛБХШЙЛЁ " +
                "ХЗИПДН ТПНЙЗ ЫЮРЮЙЗЁ СТРДЖТА. ПГПГ.", new int[]{3, 28, 10, 2, 32, 29, 1, 7, 31, 30}));

    }
}