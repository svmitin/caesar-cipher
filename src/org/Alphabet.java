package org;

public class Alphabet {
    /* Алфавит */
    char[] alphabet;

    public Alphabet() {
        /* Конструктор */
        // Преобразуем строку str в массив символов (char)
        this.alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ".toCharArray();
    }

    public int searchIndex(char symbol) {
        /* Возвращает порядковый номер символа в алфавите */
        for (int index = 0; index < alphabet.length; index++) {
            if (alphabet[index] == symbol) {
                return index;
            }
        }
        return -1; // Такой символ отсутствует в алфавите
    }

    public char changeSymbol(char symbol, int delta) {
        /* Изменить символ на дельту */
        int index = searchIndex(symbol);
        if (index < 0 ) {
            // Не удалось найти такой символ в Алфавите, оставим символ таким-же
            return symbol;
        }

        // Делаем сдвиг
        index += delta;
        while (index >= this.alphabet.length) {
            index -= this.alphabet.length;
        }

        return this.alphabet[index];
    }

}
