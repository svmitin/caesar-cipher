Требования для сборки исходного кода
модули библиотеки которые необходимо установить и откуда их скачать

Для сборки установить JDK v15 или старше на хостовую машину.
[https://www.oracle.com/java/technologies/javase-jdk16-downloads.html](https://www.oracle.com/java/technologies/javase-jdk16-downloads.html)

```
git clone https://gitlab.com/svmitin/caesar-cipher
cd caesar-cipher
mkdir build
javac -encoding utf8 -sourcepath src src/org/Main.java -d build
```

Для запуска использовать

```
java -classpath build org/Main
```